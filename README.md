# SMSH
## _A Tool for Pixel-Wise Image Compositing_

SMSH is a minimal program for compositing images together.

It supports a small set of operations which can be applied pixel-wise between two input images.


### Implementations

SMSH currently has implementations in both `C` and `Ruby`.
Take a look in the corresponding language directory for specific build/install information.  


### General Usage

To run SMSH just invoke it in your terminal:

    $ smsh diff a.png b.png c.png

The above command will combine the images `a.jpg` and `b.jpg` by applying
the `diff` operator between each corresponding pixel of the two images.
The result of this pixel-wise application of `diff` is then saved as `c.jpg`.


### Operators

The following operators are supported in SMSH:

- `addMod(a, b) → (a + b) % 256`
- `subMod(a, b) → (a - b) % 256`
- `diff(a, b) → abs(a - b)`
- `and(a, b) → a & b`
- `or(a, b) → a | b`
- `xor(a, b) → a ^ b`
- `idL(a, b) → a`
- `idR(a, b) → b`
