/* SMSH

This is a simple program that allows for the compositing of images using
a variety of operators which can be applied pixel-wise between the images.

*/

#include <string.h> // We need this for strcmp(...)

// The STB Image library is used to load images
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

// The STB Image Write library is used to write images to disk
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"


// Inline absolute value macro
#define ABS(N) (N >= 0 ? N : -1 * N)

// Type for an 8bit pixel color component
typedef unsigned char pix;

// pixel-wise operator
typedef pix (*pixOp)(pix a, pix b);

// operators:

pix OP_addMod(pix a, pix b) { return a + b; }

pix OP_subMod(pix a, pix b) { return a - b; }

pix OP_diff(pix a, pix b) { int tmp = (int)a - (int)b; return (pix)ABS(tmp); }

pix OP_and(pix a, pix b) { return a & b; }

pix OP_or(pix a, pix b) { return a | b; }

pix OP_xor(pix a, pix b) { return a ^ b; }

pix OP_idL(pix a, pix b) { return a; }

pix OP_idR(pix a, pix b) { return b; }


// Converts a string into its corresponding operator
pixOp getOp(char* str) {
  if (strcmp(str, "addMod") == 0) return &OP_addMod;
  if (strcmp(str, "subMod") == 0) return &OP_subMod;
  if (strcmp(str, "diff") == 0) return &OP_diff;
  if (strcmp(str, "and") == 0) return &OP_and;
  if (strcmp(str, "or") == 0) return &OP_or;
  if (strcmp(str, "xor") == 0) return &OP_xor;
  if (strcmp(str, "idL") == 0) return &OP_idL;
  if (strcmp(str, "idR") == 0) return &OP_idR;
  return &OP_diff; // Default to diff operator
}

// Smsh two images together with the specified operator
pix* smshImgs(pixOp op, int size, pix* imgA, pix* imgB) {
  pix* imgC = malloc(size * 3); // The return image pixels
  // Iterate over all the pixels and apply the specified operator between them
  for (int i=0; i<size*3; i++) imgC[i] = op(imgA[i], imgB[i]);
  return imgC;
}

// Program entry point
int main(int argc, char** argv) {
  // Parse operator from first program argument (first arg)
  pixOp op = getOp(argv[1]);

  // Load first input image
  char* filenameA = argv[2]; // filename of the first input image (second arg)
  int xA, yA, nA; // Dimensions and channel number for the first input image
  pix* imgA = stbi_load(filenameA, &xA, &yA, &nA, 3); // Load image from file
  // Handle bad image load
  if (imgA == NULL) {
    printf("ERROR: bad filename or corrupted first image file.\n");
    exit(1);
  }

  // Load second input image
  char* filenameB = argv[3]; // filename of the first input image (third arg)
  int xB, yB, nB; // Dimensions and channel number for the second input image
  pix* imgB = stbi_load(filenameB, &xB, &yB, &nB, 3); // Load image from file
  // Handle bad image load
  if (imgB == NULL) {
    printf("ERROR: bad filename or corrupted second image file.\n");
    exit(1);
  }

  // Handle mismatched image sizes
  if ((xA != xB) || (yA != yB)) {
    printf("ERROR: image sizes do not match\n");
    exit(1);
  }

  // Smsh images together
  pix* imgC = smshImgs(op, xA*yA, imgA, imgB);
  // N.B. We never free imgC, because this program is so simple
  // we just let it get cleaned up when the program ends

  // Write smshed image to disk
  char* filenameC = argv[4]; // Output filename (fourth arg)
  int writeSuccess = stbi_write_png(filenameC, xA, yA, 3, imgC, 0);
  // N.B. SMSH implicitly outputs PNGs

  return 0;
}
