# SMSH (_C_)


### Building/Installing

To compile the C implementation of SMSH you'll need a compiler (`GCC`, `Clang`, `TCC`, etc.) and the `make` build tool.

On a Debian/Ubuntu based distro these can be installed like so:

    $ sudo apt-get install build-essential

To build SMSH, just run:

    $ make

and to install for all users run:

    $ sudo make install


### Thanks

This program uses `stb_image.h` and `stb_image_write.h` from the excellent set of [stb libraries](https://github.com/nothings/stb).
