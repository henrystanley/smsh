# SMSH (_Ruby_)

### Installing

The Ruby implementation of SMSH has a couple dependencies:
you'll need `Ruby`, `ImageMagick`, and the `MiniMagick` gem to run it.
On a Debian/Ubuntu based distro these can be installed like so:

    $ sudo apt-get install ruby imagemagick
    $ sudo gem install mini_magick

To install SMSH for use just make it executable and toss it somewhere on your PATH:

    $ chmod +x smsh
    $ sudo cp smsh /usr/bin/smsh
